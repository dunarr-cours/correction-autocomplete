$('#autocomplete').keyup(callApi)

function afficher (data) {
  $('#completeList').html('')
  for (var i = 0; i < data.length; i++) {
    $('#completeList').append('<li>'+data[i].ville+'<small>'+data[i].cp+'</small>'+'</li>')
  }
}
function callApi() {
  $.ajax({
    url:'server.php',
    method: 'GET',
    data:{
      'q':$(this).val()
    }
  }).done(afficher)
}
