<?php
$user = 'root';
$pass = '';
try{
    $dbh = new PDO('mysql:dbname=autocom;host=127.0.0.1',$user,$pass);
} catch (Exception $e){
    var_dump($e);
}
$req = $dbh->prepare('
SELECT ville, cp
 FROM tbl_cp 
 WHERE `ville` LIKE :q 
 OR `cp` LIKE :q 
 LIMIT 5
 ');
$req->execute(['q'=>'%'.$_GET['q'].'%']);
$resultat = $req->fetchAll();
header('Content-type: application/json');
echo json_encode($resultat);
?>
